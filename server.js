var config = require('./config.json');
var fs = require('fs');
var fse = require('fs-extra');
var querystring = require('querystring');
var http = require('http');
var os = require('os');
var path = require('path');
var utils = require(config.utils);
try {
    var v8 = require('v8');
}
catch (ex) {
    console.error('v8 module not found');
}
var code = utils.code;
config.dir.path = path.normalize(__dirname + '/' + config.dir.path);

var video_file_regex = /\.webm$/;

var dirs = fs.readdirSync(config.dir.path);
var streams = {};
dirs.forEach(function (dir) {
    streams[dir] = null;
});
var buffers = {};
var clients = {};

function iterate(cb) {
    for (var j in clients) {
        cb(clients[j]);
    }
}

function serve(req, res) {
    var params = req.url.split('?');
    var route = params[0].split('/');
    params = params[1] ? querystring.parse(params[1]) : {};
    var id = route[1];
    var filename = route[2];

    if (filename && !video_file_regex.test(filename)) {
        error(code.BAD_REQUEST, 'Unsupported format');
        return;
    }

    function send(c, object) {
        if (code.OK == c || code.CREATED == c) {
            object.success = true;
        }
        res.writeHead(c, {
            'content-type': 'application/json'
        });
        if (object) {
            res.end(JSON.stringify(object));
        }
        else {
            res.end();
        }
    }

    function _object(obj) {
        var result = {};
        for (var i in obj) {
            result[i] = obj[i];
        }
        return result;
    }

    function error(c, message = {}) {
        if ('string' == typeof message) {
            message = {message: message};
        }
        if (message.error) {
            message.error = _object(message.error);
        }
        else {
            message = _object(message);
        }
        if (message.code) {
            var error_message = {
                ENOENT: 'No such directory entry',
                EACCES: 'Permission denied'
            }[message.code];
            if (error_message) {
                message.message = error_message;
            }
        }
        if (!message.error) {
            message = {error: message};
            for (i in message.error) {
                if (['id'].indexOf(i) >= 0) {
                    message[i] = message.error[i];
                    delete message.error[i];
                }
            }
        }
        message.success = false;
        send(c, message);
    }

    function megabytes(object) {
        var result = {};
        for (var i in object) {
            result[i] = object[i] / (1024 * 1024);
        }
        return result;
    }

    function is_admin() {
        return config.secret == params.secret;
    }

    var result;
    var stream;
    var buffer;

    switch (req.method) {
        case 'PUT':
            if (id) {
                if (!filename) {
                    filename = Math.round(Date.now() / 1000).toString(16);
                    filename += '.webm';
                }
                result = {
                    url: `/${id}/${filename}`,
                    filename: filename
                };
                stream = streams[id];
                if (stream) {
                    result.closed = {
                        written: stream.bytesWritten
                    };
                    stream.end();
                    streams[id] = null;
                }
                stream = fs.createWriteStream(config.dir.path + result.url, {
                    flags: 'w',
                    mode: parseInt(config.file.mode, 8)
                });
                stream.on('open', function () {
                    streams[id] = stream;
                    send(code.CREATED, result);
                });
                stream.on('error', function (err) {
                    result.error = err;
                    error(code.INTERNAL_SERVER_ERROR, result);
                });
            }
            else {
                id = utils.id12('Stream');
                var id_path = `${config.dir.path}/${id}`;
                fs.mkdir(id_path, parseInt(config.dir.mode, 8), function (err) {
                    if (err) {
                        error(code.INTERNAL_SERVER_ERROR, err);
                    }
                    else {
                        var time = /.{2}(.{16}).{6}/.exec(id);
                        time = parseInt(time[1], 16) / 1000000;
                        send(code.CREATED, {
                            id: id,
                            time: new Date(time).toISOString(),
                            path: id_path
                        });
                    }
                })
            }
            break;

        case 'POST':
            stream = streams[id];
            if (stream) {
                buffer = [];
                buffers[id] = buffer;
                req.pipe(stream, {end: false});
                req.on('data', function (chunk) {
                    iterate(c => c.write(chunk));
                    if (stream.written < config.quota) {
                        buffer.push(chunk);
                    }
                    else {
                        buffer = [];
                    }
                });
                req.on('end', function () {
                    res.end();
                    if (close) {
                        iterate(c => c.end());
                        clients = {};
                        stream.end();
                        streams[id] = null;
                    }
                })
            }
            else {
                error(code.NOT_FOUND, 'Stream not found');
            }
            break;

        case 'DELETE':
            if (is_admin()) {
                if (id) {
                    result = {
                        url: '/' + id
                    };
                    result.path = config.dir.path + result.url;
                    if (filename) {
                        result.url += '/' + filename;
                        result.path = config.dir.path + result.url;
                        stream = streams[id];
                        if (stream) {
                            stream.end();
                        }
                        fs.unlink(result.url, function (err) {
                            if (err) {
                                result.error = err;
                            }
                            send(err ? code.INTERNAL_SERVER_ERROR : code.OK, result);
                        })
                    }
                    else {
                        if (stream[id]) {
                            stream[id].end();
                        }
                        fse.remove(result.path, function (err) {
                            if (err) {
                                result.error = err;
                            }
                            send(err ? code.INTERNAL_SERVER_ERROR : code.OK, result);
                        })
                    }
                }
                else {
                    error(code.BAD_REQUEST, 'Stream id is required');
                }
            }
            else {
                error(code.FORBIDDEN, 'Only admin can delete stream');
            }
            break;

        case 'GET':
            switch (id) {
                case 'status':
                    var status = {
                        memory: {
                            usage: megabytes(process.memoryUsage()),
                            free: os.freemem() / 1000000,
                            total: os.totalmem() / 1000000
                        },
                        host: {
                            name: os.hostname()
                        },
                        time: process.hrtime(),
                        uptime: process.uptime(),
                        load: os.loadavg(),
                        cpu: {
                            usage: process.cpuUsage()
                        }
                    };
                    if (is_admin()) {
                        status.pid = process.pid;
                        status.uid = process.getuid();
                        status.gid = process.getgid();
                        status.host.uptime = os.uptime();
                        status.host.user = os.userInfo();
                        status.cpu.list = os.cpus();
                        if (v8) {
                            status.v8 = megabytes(v8.getHeapStatistics());
                            status.v8.space = v8.getHeapSpaceStatistics().map(megabytes);
                        }
                    }
                    send(code.OK, status);
                    break;

                default:
                    if (id) {
                        buffer = buffers[id];
                        stream = streams[id];
                        result = {
                            url: '/' + id
                        };
                        if (buffer) {
                            var buffer_sizes = {};
                            for (var i in buffer) {
                                buffer_sizes[i] = buffer[i].length;
                            }
                            result.buffer = buffer_sizes;
                        }
                        if (stream) {
                            result.written = stream.bytesWritten;
                        }
                        if (filename) {
                            if (video_file_regex.test(filename)) {
                                res.writeHead(code.OK, {
                                    'content-type': 'video/webm'
                                });
                                stream = fs.createReadStream(config.dir.path + result.url + '/' + filename);
                                stream.pipe(res);
                            }
                            else {
                                error(code.BAD_REQUEST, 'Unknown file format');
                            }
                        }
                        else {
                            if (req.headers['accept'].indexOf('json') > 0) {
                                fs.readdir(config.dir.path + result.url, function (err, files) {
                                    if (err) {
                                        error(code.INTERNAL_SERVER_ERROR, err);
                                    }
                                    else {
                                        result.files = files;
                                        send(code.OK, result);
                                    }
                                });
                            }
                            else {
                                function close() {
                                    delete clients[id];
                                }
                                req.on('close', close);
                                res.on('close', close);
                                res.on('finish', close);
                                clients[id] = res;
                            }
                        }
                    }
                    else {
                        send(code.OK, {
                            streams: Object.keys(streams),
                            buffers: Object.keys(buffers)
                        });
                    }
                    break;
            }
            break;

        default:
            send(code.METHOD_NOT_ALLOWED);
            break;
    }
}

var server = http.createServer(function (req, res) {
    try {
        serve(req, res);
    }
    catch (ex) {
        res.writeHead(code.INTERNAL_SERVER_ERROR, {
            'content-type': 'application/json'
        });
        res.send(JSON.stringify({error: ex}))
    }
});

server.listen(config.port, config.host);
